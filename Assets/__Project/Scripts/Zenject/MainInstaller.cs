﻿
using Zenject;

public class MainInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        // Przykład instalacji skryptu ,,ScoreManager"
        Container.Bind<LevelManager>().FromComponentInHierarchy(true).AsSingle();
        Container.Bind<TeamMenager>().FromComponentInHierarchy(true).AsSingle();
    }
}
