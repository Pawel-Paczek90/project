﻿using UnityEngine;

public class ObjectManager : MonoBehaviour
{
    public GameObject ammo;
    //public Transform target;
    public Sensor navigator;
    public ShipSpawner.Teams objectTeam;
    public Ammo ammoTeam;
    public SensorTarget sensorTarget;

    // Update is called once per frame

    private void Start()
    {
        UpdateTeam();
        CreateNewProjectile();
    }
    public void UpdateThisSpawnerTeam(ShipSpawner.Teams team)
    {
        objectTeam = team;
        navigator.UpdateObjectManager(this);
        sensorTarget.UpdateObjectManager(this);

    }
    public void UpdateTeam()
    {
        navigator.UpdateObjectManager(this);
        sensorTarget.UpdateObjectManager(this);
    }

    private void CreateNewProjectile()
    {
        if (navigator.AreTargetsInRange())
        {
            GameObject closestTarget = navigator.GetClosestTarget();
            if (closestTarget)
            {
                Transform target = closestTarget.transform;
                GameObject newAmmo = Instantiate(ammo, gameObject.transform.position, Quaternion.identity);
                newAmmo.GetComponent<Ammo>().UpDateTarget(target);
            }
        }
        Invoke(nameof(CreateNewProjectile), 0.2f);
    }
}
