﻿using UnityEngine;

public class PowerBox : MonoBehaviour
{
    public void PowerBoxMove()
    {
        transform.position = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
    }
}
