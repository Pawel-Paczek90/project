﻿using UnityEngine;

public class TowerScript : MonoBehaviour
{
    private Transform target;

    public GameObject HPbar;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<SensorTarget>() != null && other.transform == target)
        {
            HPbar.GetComponent<HPBar>().Damage(10);
        }
    }
}
