﻿using UnityEngine;

public class ShipSpawner : MonoBehaviour
{
    public GameObject[] Enemies;
    public Transform spawnBucket;
    public float spawnSpeed;
    public Teams teams;
    public ObjectManager shipManager;
    private bool energy;
    public GameObject HPbar;

    // Start is called before the first frame update



    public void ReceiveDMG(int damage)
    {
        HPbar.GetComponent<HPBar>().Damage(damage);
    }
    public enum Teams
    {
        Team1,
        Team2,
        Team3,
        Team4,
    }
    private void OnEnable()
    {
        energy = false;
        if (teams == Teams.Team2)
        {
            Invoke(nameof(SpawnEnemies), 7f);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PowerBox>() != null)
        {
            energy = true;
            Invoke(nameof(Spawn), 1f);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<PowerBox>() != null)
        {

            energy = false;
            Spawn();
        }
    }
    private void Spawn()
    {
        if (energy == true)
        {
            SpawnEnemies();

        }
        else
        {
            CancelInvoke();
        }
    }



    public void UpdateThisSpawnerTeam(Teams team)
    {
        //teams = team;
        shipManager.UpdateThisSpawnerTeam(teams);
    }


    public void SpawnEnemies()
    {
        Instantiate(Enemies[0], transform.position, Quaternion.identity);
        //GameObject randomEnemy = Instantiate(Enemies[UnityEngine.Random.Range(0, Enemies.Length)]);
        if (teams == Teams.Team2)
        {
            Invoke(nameof(SpawnEnemies), 9f);//- (Time.time / 9));
        }
        if (energy == true)
        {
            Invoke(nameof(SpawnEnemies), 2f);
        }
    }

}
