﻿using UnityEngine;

public class TeamMenager : MonoBehaviour
{
    public ShipSpawner[] Teams;
    // Start is called before the first frame update
    void Start()
    {
        Teams = FindObjectsOfType<ShipSpawner>();
        UpdateTeams();
    }

    private void UpdateTeams()
    {
        for (int i = 0; i < Teams.Length; i++)
        {
            Teams[i].UpdateThisSpawnerTeam((ShipSpawner.Teams)i);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
