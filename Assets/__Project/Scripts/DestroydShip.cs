﻿using UnityEngine;

public class DestroydShip : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)


    {
        if (other.GetComponent<ShipPivot>() != null)
        {
            Destroy(other.gameObject);
        }

    }
}
