﻿using UnityEngine;

public class Houl : MonoBehaviour
{
    public GameObject HPbar;


    // Start is called before the first frame update

    public void ReceiveDMG(int damage)
    {
        HPbar.GetComponent<HPBar>().Damage(damage);
    }
}
