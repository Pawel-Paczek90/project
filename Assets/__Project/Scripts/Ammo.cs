﻿
using UnityEngine;
public class Ammo : MonoBehaviour
{
    public int damage = 10;
    private float xvalue;
    private float yvalue;
    public float tweenTime = 0.5f;
    public Transform target;

    private void Start()
    {

        xvalue = transform.position.x;
        yvalue = transform.position.y;
    }
    private void Update()
    {
        MoveToTarget();
    }
    private void OnTriggerEnter2D(Collider2D other)


    {

        if (other.GetComponent<Houl>() != null && other.transform == target)
        {
            other.GetComponent<Houl>().ReceiveDMG(damage);
            Destroy(gameObject);
        }
        if (other.GetComponent<ShipSpawner>() != null && other.transform == target)
        {
            other.GetComponent<ShipSpawner>().ReceiveDMG(damage);
            Destroy(gameObject);
        }

    }

    public void MoveToTarget()
    {
        if (target != null)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, 5f * Time.deltaTime);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void UpDateTarget(Transform currentTarget)
    {
        target = currentTarget;
    }
}
