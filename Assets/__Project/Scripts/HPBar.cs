﻿using UnityEngine;
using UnityEngine.UI;
public class HPBar : MonoBehaviour
{
    public Image ImgHealtBar;
    public Text TextHealt;
    public GameObject shipPivot;
    public Explosion ExplosionEfect;
    public int Min;
    public int Max;

    private int mCurrentValue;
    private float mCurrentProcent;
    private void Start()
    {
        mCurrentValue = 100;
        mCurrentProcent = 100;
        SetHealth(100);
    }
    public void SetHealth(int health)
    {
        if (Min >= CurrentValue)
        {

            Destroy(shipPivot);
            Instantiate(ExplosionEfect.gameObject, new Vector3(transform.position.x, transform.position.y, 25f), Quaternion.identity);
        }
        else
        {
            mCurrentValue = health;
            mCurrentProcent = (float)mCurrentValue / (float)(Max - Min);
        }
        TextHealt.text = (mCurrentProcent * 100).ToString();
        ImgHealtBar.fillAmount = mCurrentProcent;

    }
    public void Damage(int damage)
    {
        mCurrentValue -= damage;
        SetHealth(mCurrentValue);
    }

    public float CurrentPercent
    {
        get { return mCurrentProcent; }
    }
    public int CurrentValue
    {
        get { return mCurrentValue; }
    }

    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {

    }
}
