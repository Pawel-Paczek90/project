﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    public List<GameObject> targetsInRange;
    public ShipSpawner.Teams teams;
    public SensorTarget haul;
    public DOTweenPath shipPivot;

    public ObjectManager objectManager;

    public bool init;

    public void UpdateObjectManager(ObjectManager upDateObjecManager)
    {
        objectManager = upDateObjecManager;
        init = true;
    }


    private void OnTriggerEnter2D(Collider2D other)

    {
        if (init == true)
        {
            if (other.GetComponent<SensorTarget>() != null)
            {
                if (other.GetComponent<SensorTarget>().objectManager.objectTeam != objectManager.objectTeam)
                {
                    if (shipPivot != null)
                    {
                        shipPivot.DOPause();
                    }
                    targetsInRange.Add(other.gameObject);
                }
            }
            if (other.GetComponent<ShipSpawner>() != null)
            {
                if (other.GetComponent<ShipSpawner>().teams != objectManager.objectTeam)
                {
                    if (shipPivot != null)
                    {
                        shipPivot.DOPause();
                    }
                    targetsInRange.Add(other.gameObject);
                }
            }

        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {

        if (other.GetComponent<SensorTarget>() != null)
        {
            if (other.GetComponent<SensorTarget>().objectManager.objectTeam != objectManager.objectTeam)
            {
                targetsInRange.Remove(other.gameObject);
            }
        }
        if (other.GetComponent<ShipSpawner>() != null)
        {
            if (other.GetComponent<ShipSpawner>().teams != objectManager.objectTeam)
            {
                targetsInRange.Remove(other.gameObject);
            }
        }

    }
    private void Update()
    {
        if (shipPivot != null)
        {
            if (targetsInRange.Count == 0)
                shipPivot.DOPlay();

        }

    }
    public GameObject GetClosestTarget()
    {
        float shortestDistance = Mathf.Infinity;
        GameObject targetTower = null;
        for (int i = 0; i < targetsInRange.Count; i++)
        {
            if (targetsInRange[i] != null)
            {
                float dist = Vector3.Distance(targetsInRange[i].transform.position, transform.position);
                if (dist < shortestDistance)
                {
                    shortestDistance = dist;
                    targetTower = targetsInRange[i];
                }
            }
            else
            {
                targetsInRange.RemoveAt(i);
            }
        }
        return targetTower;
    }

    public bool AreTargetsInRange()
    {
        if (targetsInRange.Count <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
